var xmlHttp = new XMLHttpRequest();
url="http://10.132.22.17:3000"


function getCall(endPoint,synchronousType) {
    theUrl=url+endPoint
    xmlHttp.open( "GET", theUrl, synchronousType );
    xmlHttp.send();
    var response=JSON.parse(xmlHttp.responseText)
    return response
}




function buildTimeLine() {
    var responseGetAllNotes=getCall("/notes",false)

     if (responseGetAllNotes["error"]!=null)
     {
         console.log("something stupid hapend, got error: " + responseGetAllNotes["error"])
     }else {
         responseGetAllNotes.success.forEach(function (element) {
             var contents=element.content,
                 contentsHeader=element.contentHeader,
                 statusClass=element.status ? "green" : "red",
                 fragment=createLearningStatusElement(contents,contentsHeader,statusClass)
             document.getElementById("timeLine").appendChild(fragment)
             console.log(element["contentHeader"])
         })

     }
}

function createLearningStatusElement(contents,contentsHeader,statusClass) {
    var frag = document.createDocumentFragment(),
        li = document.createElement('li'),
        div = document.createElement('div'),
        p_ContentHeader = document.createElement('p'),
        p_Content = document.createElement('p'),
        newContent =document.createTextNode(contents),
        newContentHeader=document.createTextNode(contentsHeader)
    li.setAttribute("type",statusClass)
    div.setAttribute("class","comment-Box")

    frag.appendChild(li).appendChild(div).appendChild(p_ContentHeader).appendChild(newContentHeader)
    frag.appendChild(li).appendChild(div).appendChild(p_Content).appendChild(newContent)

    return frag
}



function create(htmlStr) {
    var frag = document.createDocumentFragment(),
        temp = document.createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
        frag.appendChild(temp.firstChild);
    }
    return frag;
}

